<html>
	<head>
	<!-- Site should already have these or they are just for the mockup-->
		<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
		<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
		<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/foundation/5.5.0/js/foundation.min.js"></script>
		<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/foundation/5.5.0/css/normalize.min.css">
		<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/foundation/5.5.0/css/foundation.min.css">
		<link rel="stylesheet" href="css/lib/jquery-ui.css">

	<!-- these are in the js folder and you'll need to add them -->
		<script type="text/javascript" src="js/lib/jquery.timepicker.min.js"></script>
		<script type="text/javascript" src="js/lib/datepair.js"></script>
		<script type="text/javascript" src="js/lib/jquery.datepair.js"></script>
		<script type="text/javascript" src="js/lib/jquery.ui.labeledslider.min.js"></script>
		
	<!-- included in css folder, required -->
		
		<link rel="stylesheet" href="css/jquery.timepicker.css">
		<link rel="stylesheet" href="css/jquery.ui.labeledslider.min.css">
	
	<!-- customizations to style -->
		<link rel="stylesheet" href="css/main.css">
	</head>
	<body>