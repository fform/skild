$(function () {

  //Little bit to fix jquery-ui datepicker offset and foundation (probably not needed outside mock)
  $.extend($.datepicker, {
    _checkOffset: function (inst, offset, isFixed) {
      return $(this._lastInput).offset();
    }
  });

  // End Mocking

  // The save, not rated confirmation code
  $("#save-btn").on('click', function (e) {
    e.preventDefault();
    if ($("tr.un-rated").length > 0) {
      if (!confirm("There are un-scored items. Close without scoring?", "Yes", "No")) {
        return;
      }
    }
    $("#mock").html("<h1>Saved!</h1>");
  });


  //Setup arbitrary amount of rows with time pair setup
  $(".timeRow").each(function (i, e) {
    setupDateRow(e);
  });


});

var setupDateRow = function (selector) {
  //Datepair.js
  $('.time', selector).timepicker({
    'showDuration': true,
    'timeFormat': 'g:ia',

  });

  $('.date', selector).datepicker({
    //'format': 'm/d/yyyy',
    'autoclose': true,
    changeMonth: true,
    changeYear: true,
    numberOfMonths: 1
  });

  // initialize datepair
  $(selector).datepair({
    'defaultTimeDelta': 0,
    'defaultDateDelta': 7,
    parseDate: function (jq) {
      var utc = new Date($(jq).datepicker('getDate'));
      return utc && new Date(utc.getTime() + (utc.getTimezoneOffset() * 60000));
    },
    updateDate: function (el, v) {
      $(el).datepicker('setDate', v);
    }
  }).on('rangeSelected', function (e) {
    $(e.target).find('h5 span').addClass('info').text(prettyTimespan($(e.target).datepair('getTimeDiff')));
  });
};


var prettyTimespan = function (ms) {
  var sec = ms / 1000;
  var output = "";
  if (sec <= 60) {
    output = sec + " seconds";
  } else if (sec < 3600) {
    output = sec / 3600 + " minutes";
  } else if (sec < 86400) {
    output = sec / 3600 + " hours";
  } else if (sec < 604800) {
    output = Math.round(sec / 86400) + " days";
  } else {
    output = Math.round(sec / 604800) + " weeks";
  }
  return output;
};