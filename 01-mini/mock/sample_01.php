<table class="criteria">
	    <thead>
	      <tr>
	        <th class="cell01">Judging Criteria</th>
	        <th class="cell02">Assessment</th>
	        <th class="cell03">Weight</th>
	        <th class="cell04">Your Score </th>
	      </tr>
	    </thead>
	    <tbody>
	    
	    
	    
		    <tr>
		        <td title="Does the entrant have a horn?">
		        	Horn 
		        		
		        </td>
		        <td>
		          <span class="left-label" title="There are no horns.">No Horns</span>
		          <span class="right-label" title="There is one horn. ">One Horn</span>
		          <div class="slider" data-min-value="1.0" data-max-value="100.0" data-inc-value="0.25"></div>
		        </td>
		        <td class="weight">
		          <input type="text" value="50.0" id="score-weight" class="transparent" readonly />
		        </td>
		        <td class="score">
			         	<input type="text" readonly="readonly" data-max-value="10.0"  data-weight="50.0" name="rawScoreList" value="-1.0"
                	       	
                	
                   		class="na"		         	
			        
			        		        
			        
			        
                 />
		         </td>
		         <input type="hidden" name="criterionIndex" value="0" id="criterionIndex"/>
		         <input type="hidden" name="scoreCardId" value="67364" id="scoreCardId"/>
		    </tr>
		    
		    <tr>
		        <td title="Does the entrant have a horn?">
		        	Ten Choices 
		        </td>
		        <td>
		          <span class="left-label" title="There are no horns.">No Horns</span>
		          <span class="right-label" title="There is one horn. ">One Horn</span>
		          <div class="slider" data-min-value="0.0" data-max-value="10.0" data-inc-value="1.0"></div>
		        </td>
		        <td class="weight">
		          <input type="text" value="50.0" id="score-weight" class="transparent" readonly />
		        </td>
		        <td class="score">
			         	<input type="text" readonly="readonly" data-max-value="10.0"  data-weight="50.0" name="rawScoreList" value="-1.0"
                	       	
                	
                   		class="na"		         	
			        
			        		        
			        
			        
                 />
		         </td>
		         <input type="hidden" name="criterionIndex" value="0" id="criterionIndex"/>
		         <input type="hidden" name="scoreCardId" value="12345" id="scoreCardId"/>
		    </tr>
		    
	    
		    <tr>
		        <td title="How many colours do the wings have?">
		        	Wings 
		        		
		        </td>
		        <td>
		          <span class="left-label" title="">One Colour</span>
		          <span class="right-label" title="">Five Colours</span>
		          <div class="slider" data-min-value="1" data-max-value="5.0" data-inc-value="1"></div>
		        </td>
		        <td class="weight">
		          <input type="text" value="30.0" id="score-weight" class="transparent" readonly />
		        </td>
		        <td class="score">
			         	<input type="text" readonly="readonly" data-max-value="10.0"  data-weight="30.0" name="rawScoreList" value="-1.0"
                	       	
                	
                   		class="na"		         	
			        
			        		        
			        
			        
                 />
		         </td>
		         <input type="hidden" name="criterionIndex" value="1" id="criterionIndex"/>
		         <input type="hidden" name="scoreCardId" value="67364" id="scoreCardId"/>
		    </tr>
		    
		    
		    
	    
		    <tr>
		        <td title="How much like a horse is the contestant?">
		        	Horsiness 
		        		
		        </td>
		        <td>
		          <span class="left-label" title="">Not a Horse</span>
		          <span class="right-label" title="">Very Horse-like</span>
		          <div class="slider" data-min-value="0" data-max-value="1.0" data-inc-value="1.0"></div>
		        </td>
		        <td class="weight">
		          <input type="text" value="20.0" id="score-weight" class="transparent" readonly />
		        </td>
		        <td class="score">
			         	<input type="text" readonly="readonly" data-max-value="10.0"  data-weight="20.0" name="rawScoreList" value="-1.0"
                	       	
                	
                   		class="na"		         	
			        
			        		        
			        
			        
                 />
		         </td>
		         <input type="hidden" name="criterionIndex" value="2" id="criterionIndex"/>
		         <input type="hidden" name="scoreCardId" value="67364" id="scoreCardId"/>
		    </tr>
		    
		    
		    
	    
	    </tbody>
	    <tfoot>
	      <tr>
	        <th class="cell01"></th>
          <th class="cell02">Total:</th>
	        <th class="cell03"><!-- TODO: put total weight here --> 100.0<!--end TODO-->%</th>
	        <th class="cell04"><input type="text" readonly="readonly" style="background:white;" class="combinedScore" name="combinedScore" value="" /></th>
	      </tr>
	    </tfoot>
	  </table>